Recipe builder API with NEST JS

Steps to run this project:

1. Run `npm i` command
2. Setup database settings inside `ormconfig.json` file

`npm run typeorm -- migration:generate -n <name>`
`npm run typeorm -- migration:run`

3. Run `npm run seed` command, the seed represents all of the USDA data in json format.
Takes a couple of minutes to load all products into the database.

4. Run `npm start` command

Example request to interact with the API with JSON examples for Local use:

**Register**:
POST request on http://localhost:3000/api/register

{"username":"FourthUser",
"password":"Test1234%",
"email":"Test@test.com",
"firstName":"Fourth",
"lastName":"isAwesome"
}

**Login**:
POST request on http://localhost:3000/api/login
You can use existing user to login from the database seed:

{"username":"TestUser",
"password":"Test1234%"}

**Create Recipe**

POST request on http://localhost:3000/api/products/baserecipe

{	"title":"RealTestOVER9000",
	"category":"Pizza",
	"ingredients":
	[
		{
		"grams":"100",
		"productCode":"1001"
		},
		{
		"grams":"355",
		"productCode":"1017"	
		},
		{
		"grams":"255",
		"productCode":"1019"	
		}
		]
}

**Get all Base recipes**

GET request on http://localhost:3000/api/products/baserecipe











